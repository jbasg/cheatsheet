# Variables

## Con valor por defecto

```sh
FOO="${VARIABLE:=defvalue}"

AST_VERSION=$1
AST_VERSION="${AST_VERSION:-20.6.0}"
echo $AST_VERSION

```

## Si no existe el argumento del script pone la fecha de ayer por defecto

```sh
DEF_DATE=$(date --date="-1 day" +%Y%m%d)
DATE=${1:-$DEF_DATE}

```
