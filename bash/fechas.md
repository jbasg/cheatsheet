# Fechas Linux

## date

```sh
date 
```

Habituales:
```sh
date +"%Y%m%d"                      # Fecha YYYYMMDD
date --date="yesterday" +"%Y%m%d"   # Ayer con formato YYYYMMDD
date -d @1267619929 +"%Y%m%d"       # Fecha desde valor timestamp con formato YYYYMMDD
date +"%Y/%m/%d/%H"                 # Formato Folder YYYY/MM/DD/HH

$(date +"%Y%m%d")
$(date --date="yesterday" +"%Y%m%d")
$(date -d @1267619929 +"%Y%m%d")
$(date +"%Y/%m/%d/%H")

```


Con modificadores de fecha:

```sh
date --date="1 day ago"
date --date="1 week ago"
date --date="yesterday"
date --date="6 months ago"
date --date="10 sec ago"
date --date="next monday"
date --date="4 day"
date --date="tomorrow"
```

Con formato:
```sh
date +"%Y%m%d" 
date +"Year: %Y, Month: %m, Day: %d"
date +"Week number: %V Year: %y"
```


Modificadores de formato :

- %D – Display date as mm/dd/yy
- %Y – Year (e.g., 2020)
- %m – Month (01-12)
- %B – Long month name (e.g., November)
- %b – Short month name (e.g., Nov)
- %d – Day of month (e.g., 01)
- %j – Day of year (001-366)
- %u – Day of week (1-7)
- %A – Full weekday name (e.g., Friday)
- %a – Short weekday name (e.g., Fri)
- %H – Hour (00-23)
- %I – Hour (01-12)
- %M – Minute (00-59)
- %S – Second (00-60)


