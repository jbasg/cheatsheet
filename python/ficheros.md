
# Manejo ficheros


## Mover

```python
import os
import shutil

os.rename("path/to/current/file.foo", "path/to/new/destination/for/file.foo")
os.replace("path/to/current/file.foo", "path/to/new/destination/for/file.foo")
shutil.move("path/to/current/file.foo", "path/to/new/destination/for/file.foo")

```

## Copiar
```python
import shutil

shutil.copyfile(src, dst)

# 2nd option
# dst can be a folder; use shutil.copy2() to preserve timestamp
shutil.copy(src, dst) 
```


